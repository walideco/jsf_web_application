/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author walid
 */
@Entity
@Table(name = "recherches")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Recherches.findAll", query = "SELECT r FROM Recherches r")
    , @NamedQuery(name = "Recherches.findById", query = "SELECT r FROM Recherches r WHERE r.id = :id")
    , @NamedQuery(name = "Recherches.findByUser", query = "SELECT r FROM Recherches r WHERE r.user = :login")
    , @NamedQuery(name = "Recherches.findByNbExec", query = "SELECT r FROM Recherches r WHERE r.nbExec = :nbExec")})

@TableGenerator(name = "RECHERCHE_GEN",
        table = "SEQUENCE",
        pkColumnName = "SEQ_NAME",
        valueColumnName = "SEQ_NUMBER",
        pkColumnValue = "RECHERCHE_ID",
        allocationSize = 1)
public class Recherches implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "RECHERCHE_GEN")
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "nb_exec")
    private Integer nbExec;
    @Size(max = 45)
    @Column(name = "exec_date")
    private String execDate;
    @JoinColumn(name = "user", referencedColumnName = "login")
    @ManyToOne
    private Utilisateurs user;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "rechercheId")
    private List<Criteres> criteresList;

    public Recherches() {
        nbExec = 0;
        criteresList = new ArrayList<>();
    }

    public Recherches(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNbExec() {
        return nbExec;
    }

    public void setNbExec(Integer nbExec) {
        this.nbExec = nbExec;
    }

    public String getExecDate() {
        return execDate;
    }

    public void setExecDate(String execDate) {
        this.execDate = execDate;
    }

    public Utilisateurs getUser() {
        return user;
    }

    public void setUser(Utilisateurs user) {
        this.user = user;
    }

    @XmlTransient
    public List<Criteres> getCriteresList() {
        return criteresList;
    }

    public void setCriteresList(List<Criteres> criteresList) {
        this.criteresList = criteresList;
    }

    public String getCritereOnString() {
        return criteresList.stream().map((c) -> c.getEtiquette()).collect(Collectors.joining(", "));
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Recherches)) {
            return false;
        }
        Recherches other = (Recherches) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Recherches[ id=" + id + " ]";
    }

}
