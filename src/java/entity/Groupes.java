/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author walid
 */
@Entity
@Table(name = "groupes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Groupes.findAll", query = "SELECT g FROM Groupes g")
    , @NamedQuery(name = "Groupes.findByLogin", query = "SELECT g FROM Groupes g WHERE g.groupesPK.login = :login")
    , @NamedQuery(name = "Groupes.findByGroupe", query = "SELECT g FROM Groupes g WHERE g.groupesPK.groupe = :groupe")})
public class Groupes implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GroupesPK groupesPK;

    public Groupes() {
    }

    public Groupes(GroupesPK groupesPK) {
        this.groupesPK = groupesPK;
    }

    public Groupes(String login, String groupe) {
        this.groupesPK = new GroupesPK(login, groupe);
    }

    public GroupesPK getGroupesPK() {
        return groupesPK;
    }

    public void setGroupesPK(GroupesPK groupesPK) {
        this.groupesPK = groupesPK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (groupesPK != null ? groupesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Groupes)) {
            return false;
        }
        Groupes other = (Groupes) object;
        if ((this.groupesPK == null && other.groupesPK != null) || (this.groupesPK != null && !this.groupesPK.equals(other.groupesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Groupes[ groupesPK=" + groupesPK + " ]";
    }
    
}
