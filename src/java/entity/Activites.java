/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author walid
 */
@Entity
@Table(name = "activites")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Activites.findAll", query = "SELECT a FROM Activites a")
    , @NamedQuery(name = "Activites.findById", query = "SELECT a FROM Activites a WHERE a.id = :id")
    , @NamedQuery(name = "Activites.findByActCode", query = "SELECT a FROM Activites a WHERE a.actCode = :actCode")
    , @NamedQuery(name = "Activites.findByActLib", query = "SELECT a FROM Activites a WHERE a.actLib = :actLib")
    , @NamedQuery(name = "Activites.findByActNivLib", query = "SELECT a FROM Activites a WHERE a.actNivLib = :actNivLib")
    , @NamedQuery(name = "Activites.findByComInsee", query = "SELECT a FROM Activites a WHERE a.comInsee = :comInsee")
    , @NamedQuery(name = "Activites.findByComLib", query = "SELECT a FROM Activites a WHERE a.comLib = :comLib")
    , @NamedQuery(name = "Activites.findByDepCode", query = "SELECT a FROM Activites a WHERE a.depCode = :depCode")
    , @NamedQuery(name = "Activites.findByDepLib", query = "SELECT a FROM Activites a WHERE a.depLib = :depLib")
    , @NamedQuery(name = "Activites.findByEquActivitePraticable", query = "SELECT a FROM Activites a WHERE a.equActivitePraticable = :equActivitePraticable")
    , @NamedQuery(name = "Activites.findByEquActivitePratique", query = "SELECT a FROM Activites a WHERE a.equActivitePratique = :equActivitePratique")
    , @NamedQuery(name = "Activites.findByEquActiviteSalleSpe", query = "SELECT a FROM Activites a WHERE a.equActiviteSalleSpe = :equActiviteSalleSpe")
    , @NamedQuery(name = "Activites.findByActNivLib", query = "SELECT a FROM Activites a WHERE a.actNivLib = :actNivLib")})

@TableGenerator(name = "ACTIVITE_GEN",
                table = "SEQUENCE",
		pkColumnName = "SEQ_NAME",
                valueColumnName = "SEQ_NUMBER",
		pkColumnValue = "ACTIVITE_ID",
                allocationSize=1)
public class Activites implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "ACTIVITE_GEN")
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 255)
    @Column(name = "ActCode")
    private String actCode;
    @Size(max = 255)
    @Column(name = "ActLib")
    private String actLib;
    @Size(max = 255)
    @Column(name = "ActNivLib")
    private String actNivLib;
    @Size(max = 255)
    @Column(name = "ComInsee")
    private String comInsee;
    @Size(max = 255)
    @Column(name = "ComLib")
    private String comLib;
    @Size(max = 255)
    @Column(name = "DepCode")
    private String depCode;
    @Size(max = 255)
    @Column(name = "DepLib")
    private String depLib;
    @Size(max = 255)
    @Column(name = "EquActivitePraticable")
    private String equActivitePraticable;
    @Size(max = 255)
    @Column(name = "EquActivitePratique")
    private String equActivitePratique;
    @Size(max = 255)
    @Column(name = "EquActiviteSalleSpe")
    private String equActiviteSalleSpe;
    @Size(max = 255)
    @Column(name = "EquNbEquIdentique")
    private String equNbEquIdentique;
    @Size(max = 255)
    @Column(name = "EquipementId")
    private String equipementId;

    public Activites() {
    }

    public Activites(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getActCode() {
        return actCode;
    }

    public void setActCode(String actCode) {
        this.actCode = actCode;
    }

    public String getActLib() {
        return actLib;
    }

    public void setActLib(String actLib) {
        this.actLib = actLib;
    }

    public String getActNivLib() {
        return actNivLib;
    }

    public void setActNivLib(String actNivLib) {
        this.actNivLib = actNivLib;
    }

    public String getComInsee() {
        return comInsee;
    }

    public void setComInsee(String comInsee) {
        this.comInsee = comInsee;
    }

    public String getComLib() {
        return comLib;
    }

    public void setComLib(String comLib) {
        this.comLib = comLib;
    }

    public String getDepCode() {
        return depCode;
    }

    public void setDepCode(String depCode) {
        this.depCode = depCode;
    }

    public String getDepLib() {
        return depLib;
    }

    public void setDepLib(String depLib) {
        this.depLib = depLib;
    }

    public String getEquActivitePraticable() {
        return equActivitePraticable;
    }

    public void setEquActivitePraticable(String equActivitePraticable) {
        this.equActivitePraticable = equActivitePraticable;
    }

    public String getEquActivitePratique() {
        return equActivitePratique;
    }

    public void setEquActivitePratique(String equActivitePratique) {
        this.equActivitePratique = equActivitePratique;
    }

    public String getEquActiviteSalleSpe() {
        return equActiviteSalleSpe;
    }

    public void setEquActiviteSalleSpe(String equActiviteSalleSpe) {
        this.equActiviteSalleSpe = equActiviteSalleSpe;
    }

    public String getEquNbEquIdentique() {
        return equNbEquIdentique;
    }

    public void setEquNbEquIdentique(String equNbEquIdentique) {
        this.equNbEquIdentique = equNbEquIdentique;
    }

    public String getEquipementId() {
        return equipementId;
    }

    public void setEquipementId(String equipementId) {
        this.equipementId = equipementId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Activites)) {
            return false;
        }
        Activites other = (Activites) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Activites[ id=" + id + " ]";
    }
    
}
