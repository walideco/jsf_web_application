/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author walid
 */
@Entity
@Table(name = "criteres")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Criteres.findAll", query = "SELECT c FROM Criteres c")
    , @NamedQuery(name = "Criteres.findById", query = "SELECT c FROM Criteres c WHERE c.id = :id")
    , @NamedQuery(name = "Criteres.findByChampsDonnee", query = "SELECT c FROM Criteres c WHERE c.champsDonnee = :champsDonnee")
    , @NamedQuery(name = "Criteres.findByEtiquette", query = "SELECT c FROM Criteres c WHERE c.etiquette = :etiquette")
    , @NamedQuery(name = "Criteres.findByMode", query = "SELECT c FROM Criteres c WHERE c.mode = :mode")
    , @NamedQuery(name = "Criteres.findByValeur", query = "SELECT c FROM Criteres c WHERE c.valeur = :valeur")})

@TableGenerator(name = "CRITERE_GEN",
                table = "SEQUENCE",
		pkColumnName = "SEQ_NAME",
                valueColumnName = "SEQ_NUMBER",
		pkColumnValue = "CRITERE_ID",
                allocationSize=1)
public class Criteres implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "CRITERE_GEN")
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "champs_donnee")
    private String champsDonnee;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "etiquette")
    private String etiquette;
    @Basic(optional = false)
    @NotNull
    @Column(name = "mode")
    private int mode;
    @Size(max = 100)
    @Column(name = "valeur")
    private String valeur;
    @JoinColumn(name = "recherche_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Recherches rechercheId;

    public Criteres() {
    }

    public Criteres(Integer id) {
        this.id = id;
    }

    public Criteres(Integer id, String champsDonnee, String etiquette, int mode) {
        this.id = id;
        this.champsDonnee = champsDonnee;
        this.etiquette = etiquette;
        this.mode = mode;
    }

    public Criteres(String champsDonnee, String etiquette, int mode, String valeur, Recherches recherche) {
        this.champsDonnee = champsDonnee;
        this.etiquette = etiquette;
        this.mode = mode;
        this.rechercheId = recherche;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChampsDonnee() {
        return champsDonnee;
    }

    public void setChampsDonnee(String champsDonnee) {
        this.champsDonnee = champsDonnee;
    }

    public String getEtiquette() {
        return etiquette;
    }

    public void setEtiquette(String etiquette) {
        this.etiquette = etiquette;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public String getValeur() {
        return valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    public Recherches getRechercheId() {
        return rechercheId;
    }

    public void setRechercheId(Recherches rechercheId) {
        this.rechercheId = rechercheId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Criteres)) {
            return false;
        }
        Criteres other = (Criteres) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Criteres[ id=" + id + " ]";
    }

}
