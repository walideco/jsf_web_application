package controller;

import entity.Activites;
import entity.Criteres;
import entity.Recherches;
import entity.Utilisateurs;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpSession;

@Stateless
@LocalBean
public class RechercheManager {

    @PersistenceContext(unitName = "EASportPU")
    private EntityManager em;

    public List<Activites> execute(Recherches recherche) {
        String user = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
        recherche.setUser(em.find(Utilisateurs.class, user));
        recherche.setExecDate(getDateTime());
        recherche.setNbExec(recherche.getNbExec() + 1);

        List<Criteres> criteres = recherche.getCriteresList();
        Query query = em.createQuery("SELECT a FROM Activites a" + getWhereClause(criteres));
        for (Criteres critere : criteres) {
            String value = (critere.getValeur() == null) ? "" : critere.getValeur();
            query.setParameter(critere.getChampsDonnee(), value);
        }
        List<Activites> result = query.setMaxResults(2000).getResultList();
        if (recherche.getId() != null && em.find(Recherches.class, recherche.getId()) != null) {
            update(recherche);
        } else {
            persist(recherche);
        }
        em.flush();
        return result;
    }

    public void supprimer(int id) {
        Recherches recherche = em.find(Recherches.class, id);
        if (recherche != null) {
            em.remove(recherche);
            em.flush();
        }
    }

    public void persist(Object object) {
        em.persist(object);
    }

    public Recherches update(Recherches recherches) {
        return em.merge(recherches);
    }

    public Recherches findRecherche(int id) {
        return em.find(Recherches.class, id);
    }

    public List<Recherches> findHistorique(String userLogin) {
        Utilisateurs user = em.find(Utilisateurs.class, userLogin);
        if (user == null) {
            return new ArrayList<>();
        }
        return em.createQuery("SELECT r FROM Recherches r WHERE r.user = :user")
                .setParameter("user", user)
                .getResultList();
    }

    private String getDateTime() {
        Calendar cal = new GregorianCalendar();
        Date date = cal.getTime();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy' 'HH:mm:ss");
        return format.format(date);
    }

    private String getWhereClause(List<Criteres> criteres) {
        String clause = " WHERE ";
        if (criteres.isEmpty()) {
            return "";
        }
        int size = criteres.size();
        for (int i = 0; i < criteres.size() - 1; i++) {
            clause = clause + " a."
                    + criteres.get(i).getChampsDonnee()
                    + " = :" + criteres.get(i).getChampsDonnee()
                    + " AND ";
        }
        clause = clause + " a." + criteres.get(size - 1).getChampsDonnee()
                + "= :" + criteres.get(size - 1).getChampsDonnee();
        return clause;
    }

    public List<Object[]> findTopFiveCritere() {
        Query query = em.createQuery(
                "SELECT c.etiquette,  count(c.id) AS ct  FROM Criteres AS c GROUP BY c.etiquette ORDER BY ct DESC");
        List<Object[]> results = query.setMaxResults(5).getResultList();
        return results;
    }
}
