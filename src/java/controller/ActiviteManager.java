/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Activites;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class ActiviteManager {

    @PersistenceContext(unitName = "EASportPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    public Activites update(Activites activite) {
        activite = em.merge(activite);
        em.flush();
        return activite;
    }

    public void delete(Activites activite) {
        em.remove(activite);
    }

    public List<Object[]> findTopFiveActDep() {
        Query query = em.createQuery(
                "SELECT a.depCode, a.depLib,  count(a.id) AS c  FROM Activites AS a GROUP BY a.depCode, a.depLib ORDER BY c DESC");
        List<Object[]> results = query.setMaxResults(5).getResultList();
        return results;
    }

    public List<Object[]> findTopFiveAct() {
        Query query = em.createQuery(
                "SELECT a.actCode, a.actLib,  count(DISTINCT a.depCode) AS c  FROM Activites AS a GROUP BY a.actCode, a.actLib ORDER BY c ASC");
        List<Object[]> results = query.setMaxResults(10).getResultList();
        return results;
    }

    public List<Object[]> findTopFiveEquipPolyv() {
        Query query = em.createQuery(
                "SELECT a.equipementId, count(DISTINCT a.depCode) AS c  FROM Activites AS a GROUP BY a.equipementId ORDER BY c DESC");
        List<Object[]> results = query.setMaxResults(5).getResultList();
        return results;
    }

}
