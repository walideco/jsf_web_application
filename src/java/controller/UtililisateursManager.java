/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Activites;
import entity.Utilisateurs;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author walid
 */
@Stateless
public class UtililisateursManager {

    @PersistenceContext(unitName = "EASportPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    public Utilisateurs update(Utilisateurs utilisateur) {
        utilisateur = em.merge(utilisateur);
        em.flush();
        return utilisateur;
    }

    public void delete(Utilisateurs utilisateur) {
        em.remove(utilisateur);
    }

    public Utilisateurs findByLogin(String login) {
        return em.find(Utilisateurs.class, login);
    }
}
