/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import controller.RechercheManager;
import controller.ActiviteManager;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@Named
@SessionScoped
public class statistiqueBean {

    @EJB
    private ActiviteManager activiteManager;
    @EJB
    private RechercheManager rechercheManager;

    public statistiqueBean() {
    }

    public List<Object[]> getTopFiveActDep() {
        return activiteManager.findTopFiveActDep();
    }

    public List<Object[]> getTopFiveAct() {
        return activiteManager.findTopFiveAct();
    }

    public List<Object[]> getTopFiveCritere() {
        return rechercheManager.findTopFiveCritere();
    }

    public List<Object[]> getTopFiveequipPolyv() {
        return activiteManager.findTopFiveEquipPolyv();
    }
}
