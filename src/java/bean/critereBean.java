package bean;

import entity.Criteres;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;

@ManagedBean
@Named
@SessionScoped
public class critereBean implements Serializable{

    private Criteres critere;

    public Criteres getCritere() {
        return critere;
    }

    public void setCritere(Criteres critere) {
        this.critere = critere;
    }
    
    
}
