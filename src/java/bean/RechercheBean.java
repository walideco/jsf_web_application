package bean;

import controller.RechercheManager;
import entity.Activites;
import entity.Criteres;
import entity.Recherches;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

@ManagedBean
@Named
@SessionScoped
public class RechercheBean implements Serializable {

    @EJB
    private RechercheManager rechercheManager;

    private Recherches recherche;
    private List<Activites> result;

    public RechercheBean() {
        result = new ArrayList<>();
        recherche = new Recherches();
    }

    public Recherches getRecherche() {
        return recherche;
    }

    public void setRecherche(Recherches recherche) {
        this.recherche = recherche;
    }

    public List<Activites> getResult() {
        return result;
    }

    public void setResult(List<Activites> result) {
        this.result = result;
    }

    public List<Recherches> getHstorique(String user) {
        return (user == null)? new ArrayList<>() : rechercheManager.findHistorique(user);
    }

    public String executeRecherche() {
        result = rechercheManager.execute(recherche);
        return "/users/resultatRecherche";
    }

    public String afficherRecherche(int id) {
        recherche = rechercheManager.findRecherche(id);
        return "/users/recherche";
    }

    public String suppRecherche(int id) {
        rechercheManager.supprimer(id);
        return "/users/historique";
    }

    public String initRecherche() {
        recherche = new Recherches();
        return "/users/recherche";
    }

    private void majCriteres(Criteres criteres) {
        List<Criteres> ls = recherche.getCriteresList();
        if (ls.stream().filter(c -> c.getChampsDonnee().equals(criteres.getChampsDonnee())).count() == 0) {
            ls.add(criteres);
            recherche.setCriteresList(ls);
        }
    }

    public String ajouterCritere(ValueChangeEvent event) {
        int value = Integer.valueOf(event.getNewValue().toString());
        switch (value) {
            case 1:
                majCriteres(new Criteres("actLib", "Libelé", 0, "", recherche));
                break;
            case 2:
                majCriteres(new Criteres("actNivLib", "Niveau", 0, "", recherche));
                break;
            case 3:
                majCriteres(new Criteres("comLib", "Commune", 0, "", recherche));
                break; 
            case 4:
                majCriteres(new Criteres("depLib", "Departement", 0, "", recherche));
                break;
            case 5:
                majCriteres(new Criteres("equActivitePraticable", "Praticable", 0, "", recherche));
                break;
            case 6:
                majCriteres(new Criteres("equActivitePratique", "Pratiquée", 0, "", recherche));
                break;
            case 7:
                majCriteres(new Criteres("equipementId", "Equipement ID", 0, "", recherche));
                break;
            case 8:
                break;
            case 9:
                break;
            default: ;
                break;
        }
        return "/users/recherche";
    }

    public String supprimerCritere(String arg) {
        recherche.getCriteresList().removeIf(c -> c.getChampsDonnee().equals(arg));
        return "/users/recherche";
    }

    public List<String> getPossibleCritere() {
        List<String> ls = Arrays.asList(new String[]{"1 Libelé", "2 Niveau", "3 Commune", "4 Departement", "5 Praticable", "6 Pratiquée", "7 Equipement ID"});
        List<String> used = recherche.getCriteresList().stream().map(c -> c.getEtiquette()).collect(Collectors.toList());
        List<String> tmp = ls.stream().filter(e -> !used.contains(e.substring(2))).collect(Collectors.toList());;
        return tmp;
    }
}
