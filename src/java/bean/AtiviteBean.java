package bean;

import controller.ActiviteManager;
import entity.Activites;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;

@ManagedBean
@Named
@SessionScoped
public class AtiviteBean {

    @EJB
    private ActiviteManager activiteManager;
    private Activites activite;

    public Activites getActivite() {
        return activite;
    }

    public void setActivite(Activites activite) {
        this.activite = activite;
    }

    public String afficher(Activites activite) {
        this.activite = activite;
        return "/users/activite";
    }

    public String enregistrer() {
        activite = activiteManager.update(activite);
        return "/users/activite";
    }
    
    public String supprimer(Activites activite) {
        this.activiteManager.delete(activite);
        return "/users/recherche";
    }

 // TODO liste Oui / Non dans formulaire
}