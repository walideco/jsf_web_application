/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import controller.UtililisateursManager;
import entity.Groupes;
import entity.GroupesPK;
import entity.Utilisateurs;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.util.Base64;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityExistsException;

@ManagedBean
@Named(value = "utilisateursBean")
@RequestScoped
public class UtilisateursBean implements Serializable {

    private Utilisateurs utilisateur;
    private String confMotDePasse;
    private String tmpLogin;

    @EJB
    private UtililisateursManager utililisateursManager;

    public UtilisateursBean() {
        utilisateur = new Utilisateurs();
    }

    public Utilisateurs getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateurs utilisateur) {
        this.utilisateur = utilisateur;
    }

    public String getConfMotDePasse() {
        return confMotDePasse;
    }

    public void setConfMotDePasse(String confMotDePasse) {
        this.confMotDePasse = confMotDePasse;
    }

    public String getTmpLogin() {
        return tmpLogin;
    }

    public void setTmpLogin(String tmpLogin) {
        this.tmpLogin = tmpLogin;
    }

    public String register() {
        if (!validate()) {
            return "/inscription";
        }
        try {

            System.out.println(MotDePasseSha256());
            utilisateur.setPassword(MotDePasseSha256());
            utililisateursManager.persist(utilisateur);
            utililisateursManager.persist(new Groupes(utilisateur.getLogin(), "user"));
        } catch (NoSuchAlgorithmException e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage("Votre inscription a échouée ... Rééssayez plus tard "));
            return "/inscription";
        }
        return "inscriptionConfirmation";
    }

    private String MotDePasseSha256() throws NoSuchAlgorithmException {
        MessageDigest mDigest = MessageDigest.getInstance("SHA-256");
        byte[] bytes = utilisateur.getPassword().getBytes();
        byte[] result = mDigest.digest(bytes);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < result.length; i++) {
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    private boolean validate() {
        if (tmpLogin == null || tmpLogin.length() < 5) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage("Le Login doit contenir au moins 5 caractère"));
            return false;
        }
        if (utililisateursManager.findByLogin(tmpLogin) != null) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage("Le login que vous avez choisi est déjà utislisé  "));
            return false;
        }
        utilisateur.setLogin(tmpLogin);
        if (utilisateur.getPassword() == null || utilisateur.getPassword().length() < 5) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage("Le mot de passe doit contenir au moins 5 caractère"));
            return false;
        }
        if (!utilisateur.getPassword().equals(confMotDePasse)) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage("Confirmation du mot de passe non valide"));
            return false;
        }
        return true;
    }
}
